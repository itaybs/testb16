<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bonusreason".
 *
 * @property integer $id
 * @property string $name
 */
class Bonusreason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusreason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	
	public static function getBonusreason()
	{
		$allBonusreasons = self::find()->all();
		$allBonusreasonsArray = ArrayHelper::
					map($allBonusreasons, 'id', 'name');
		return $allBonusreasonsArray;						
	}
	
}
